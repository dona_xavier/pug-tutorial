/**
 * Required External Modules
 */
const express = require('express');
const path = require('path');

/**
 * App Variables
 */
const app = express();
const port = process.env.PORT || 3000

/**
 *  App Configuration
 */
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
app.use(express.static(path.join(__dirname, "public")));
/**
 * Routes Definitions
 */
app.get('/', (req, res) => {
    var products = [
        {
            "title": "Skin & Coat",
            "price": "$20 - $25",
            "description": "Premium support for healthy skin and shiny coat",
            "image": "images/product1.jpg",
        },
        {
            "title": "Skin & Coat",
            "price": "$30 - $45",
            "description": "Premium support for healthy skin and shiny coat",
            "image": "images/product2.jpg",
        },
        {
            "title": "Skin & Coat",
            "price": "$40 - $55",
            "description": "Premium support for healthy skin and shiny coat",
            "image": "images/product3.jpg",
        },
        {
            "title": "Skin & Coat",
            "price": "$50 - $65",
            "description": "Premium support for healthy skin and shiny coat",
            "image": "images/product4.jpg",
        },
    ]
    res.render("index", {title: "Home", products: products});
})

app.get('/products', (req, res) => {
    var products = [
        {
            "title": "Skin & Coat",
            "price": "$20 - $25",
            "description": "Premium support for healthy skin and shiny coat",
            "image": "images/product1.jpg",
        },
        {
            "title": "Skin & Coat",
            "price": "$30 - $45",
            "description": "Premium support for healthy skin and shiny coat",
            "image": "images/product2.jpg",
        },
        {
            "title": "Skin & Coat",
            "price": "$40 - $55",
            "description": "Premium support for healthy skin and shiny coat",
            "image": "images/product3.jpg",
        },
        {
            "title": "Skin & Coat",
            "price": "$50 - $65",
            "description": "Premium support for healthy skin and shiny coat",
            "image": "images/product4.jpg",
        },
    ]
    res.render("products", {title: "Products", products: products})
})

/**
 * Server Activation
 */
app.listen(port, ()=> {
    console.log(`PORT: ${port}`)
})